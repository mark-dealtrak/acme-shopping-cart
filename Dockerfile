FROM composer:latest AS composer

FROM php:7.4-cli

ENV TERM linux
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y -q zip unzip

RUN mkdir -p /app/composer /usr/src/app \
 && addgroup -gid 1000 app \
 && useradd -u 1000 -g app -s /bin/bash -d /app app \
 && chown -R app:app /app/ /usr/src/app/

ENV COMPOSER_HOME /app/composer
ENV PATH "/app/composer/vendor/bin:$PATH"
ENV COMPOSER_ALLOW_SUPERUSER 1
COPY --from=composer /usr/bin/composer /usr/bin/composer

USER app

WORKDIR /usr/src/app