# Install

docker build . -t acme-shopping-cart

docker run -v ${PWD}:/usr/src/app -it --rm acme-shopping-cart composer install

# Create specs

docker run -v ${PWD}:/usr/src/app -it --rm acme-shopping-cart bin/phpspec desc "Acme\Cart"

docker run -v ${PWD}:/usr/src/app -it --rm acme-shopping-cart bin/phpspec desc "Acme\CartItem"

# Run tests 
docker run -v ${PWD}:/usr/src/app -it --rm acme-shopping-cart bin/phpspec run --format=pretty

