<?php

namespace Acme;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Cart
 * @package Acme
 */
class Cart
{
    private ArrayCollection $items;
    private float $shipping = 0;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Returns the items in the cart.
     *
     * @return ArrayCollection
     */
    public function getItems(): ArrayCollection
    {
        return  $this->items;
    }

    /**
     * Returns the total of the items in the cart.
     */
    public function getItemsTotal()
    {
        $total = 0;

        /** @var CartItem $item */
        foreach ($this->items as $item) {
            $total += $item->getPrice() * $item->getQuantity();
        }

        return $total;
    }

    /**
     * Returns an item in the cart.
     *
     * @param CartItem $item
     *
     * @return CartItem|null
     */
    public function getItem(CartItem $item):? CartItem
    {
        return $this->items->get($item->getKey());
    }

    /**
     * Add an item to the cart. If the item already exists, update the quantity.
     *
     * @param CartItem $item
     */
    public function addItem(CartItem $item)
    {
        if ($this->getItem($item) instanceof CartItem) {
            $currentItem = $this->getItem($item);
            $item->setQuantity($currentItem->getQuantity() + $item->getQuantity());
        }

        $this->items->set($item->getKey(), $item);
    }

    /**
     * Sets the cart shipping cost.
     *
     * @param float $shipping
     */
    public function setShipping(float $shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * Returns the cart total including the items plus shipping.
     */
    public function getTotal()
    {
        return $this->getItemsTotal() + $this->shipping;
    }
}
