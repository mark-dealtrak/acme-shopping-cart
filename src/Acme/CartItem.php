<?php

namespace Acme;

/**
 * Class CartItem
 * @package Acme
 */
class CartItem
{
    private string $key;
    private string $name;
    private int $quantity;
    private float $price;

    /**
     * @param string $key
     * @param string $name
     * @param int    $quantity
     * @param float  $price
     */
    public function __construct(string $key, string $name, int $quantity, float $price)
    {
        $this->key = $key;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    /**
     * Returns an item's key.
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * Returns an item's name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns an item's quantity.
     *
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Set an item's quantity.
     *
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Returns an item's price.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}