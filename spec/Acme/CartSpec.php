<?php

namespace spec\Acme;

use Acme\Cart;
use Acme\CartItem;
use PhpSpec\ObjectBehavior;

class CartSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Cart::class);
    }

    public function it_can_add_a_cart_item_to_the_cart(CartItem $item)
    {
        $item->getKey()->willReturn('egg1');
        $item->getName()->willReturn('ACME Egg Timer');
        $item->getQuantity()->willReturn(1);
        $item->getPrice()->willReturn(3.99);

        $this->addItem($item);

        $this->getItem($item)->shouldReturn($item);
    }

    public function it_should_increase_quantity_when_same_cart_item_is_added(CartItem $item)
    {
        $item->getKey()->willReturn('test1');
        $item->getName()->willReturn('A test product');
        $item->getQuantity()->willReturn(1);
        $item->getPrice()->willReturn(1.99);

        $this->addItem($item);
        $this->addItem($item);

        $item->setQuantity(2)->shouldHaveBeenCalled();
    }

    public function it_can_add_multiple_cart_items_to_the_cart(CartItem $item1, CartItem $item2)
    {
        $item1->getKey()->willReturn('test1');
        $item1->getName()->willReturn('A test product');
        $item1->getQuantity()->willReturn(1);
        $item1->getPrice()->willReturn(1.99);

        $item2->getKey()->willReturn('test2');
        $item2->getName()->willReturn('Another test product');
        $item2->getQuantity()->willReturn(1);
        $item2->getPrice()->willReturn(5.99);

        $this->addItem($item1);
        $this->addItem($item2);

        $this->getItem($item1)->shouldReturn($item1);
        $this->getItem($item2)->shouldReturn($item2);
    }

    public function it_returns_the_correct_total(CartItem $item1)
    {
        $item1->getKey()->willReturn('bros_greatest_hits');
        $item1->getName()->willReturn('Bros Greatest Hits');
        $item1->getQuantity()->willReturn(2);
        $item1->getPrice()->willReturn(9.99);

        $this->addItem($item1);

        $this->getTotal()->shouldEqual(19.98);
    }

    public function it_returns_the_correct_total_with_shipping(CartItem $item1)
    {
        $item1->getKey()->willReturn('test1');
        $item1->getName()->willReturn('A test product');
        $item1->getQuantity()->willReturn(1);
        $item1->getPrice()->willReturn(1.99);

        $this->addItem($item1);
        $this->setShipping(0.99);

        $this->getTotal()->shouldEqual(2.98);
    }

    public function it_can_set_shipping()
    {
        $this->setShipping(10.00);
        $this->getTotal()->shouldEqual(10.00);
    }
}