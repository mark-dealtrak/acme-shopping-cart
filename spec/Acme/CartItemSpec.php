<?php

namespace spec\Acme;

use Acme\CartItem;
use PhpSpec\ObjectBehavior;

class CartItemSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('testkey', 'test item', 1, 1.00);
    }

    public function it_is_initializable()
    {
        $this->shouldHaveType(CartItem::class);
    }

    public function it_has_a_key()
    {
        $this->getKey()->shouldReturn('testkey');
    }

    public function it_has_a_name()
    {
        $this->getName()->shouldReturn('test item');
    }

    public function it_has_a_quantity()
    {
        $this->getQuantity()->shouldReturn(1);
    }

    public function it_has_a_price()
    {
        $this->beConstructedWith('testkey', 'test item', 1, 2.00);
        $this->getPrice()->shouldReturn(2.00);
    }

    public function it_can_set_a_quantity()
    {
        $this->setQuantity(5);
        $this->getQuantity()->shouldReturn(5);
    }
}